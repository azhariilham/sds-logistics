@extends('mail_layout')

@section('title', 'Order pengiriman SDS Logistics')

@section('content')
    <table class="table">
        <tr><th scope="row">Nama Lengkap</th><td>{{$order->namalengkap}}</td></tr>
        <tr><th scope="row">No. Telepon</th><td>{{$order->telepon}}</td></tr>
        <tr><th scope="row">No. Hp / WA</th><td>{{$order->hpwa}}</td></tr>
        <tr><th scope="row">E-Mail</th><td>{{$order->email}}</td></tr>
        <tr><th scope="row">Jenis Barang</th><td>{{$order->jenisbarang}}</td></tr>
        <tr><th scope="row">Dimensi Barang</th><td>{{$order->dimensi}}</td></tr>
        <tr><th scope="row">Alamat Tujuan</th><td>{{$order->alamattujuan}}<br>{{$order->kecamatantujuan}} Kode Pos : {{$order->kodepostujuan}}</td></tr>
        <tr><th scope="row">Alamat Pickup</th><td>{{$order->alamatpickup}}<br>{{$order->kecamatanpickup}} Kode Pos : {{$order->kodepospickup}}</td></tr>
    </table>
@endsection