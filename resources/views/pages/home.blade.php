@extends('main')

@section('styles')
<style type="text/css">
    .card {margin: 10px; min-height: 22rem; border-radius: 0; border: 10px solid #2560a6;} .carousel-img {display: inline-block; position: relative;} .carousel-img::after {content: ''; position: absolute; top: 0; bottom: 0; left: 0; right: 0; box-shadow: 0 -150px 60px 0 rgba(0,0,0,.8) inset;} .tarif-row {margin-top: -10px;} .tarif-col {padding-top: 20px; padding-bottom: 40px; margin-bottom: 20px; background: #2560a6;} .cek-tarif { max-width: 800px; color: #fff; text-align: center;} .cek-tarif hr {background: rgba(255,255,255,.7) 2px;} .card-icon {padding: 30px; font-size: 5rem; color: #2560a6; } .bluesds {background: #2560a6;} h1.ourclient, h1.ourpartner, h1.video-title {text-align: center; color: #2560a6;}
    @media only screen and (max-width: 480px) {.carousel-img::after {box-shadow: none;}}
</style>
@endsection

@section('content')
    <div class="row">
        <div class="col">
            <div id="homeCarousel" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#homeCarousel" data-slide-to="1"></li>
                    <li data-target="#homeCarousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="carousel-img">
                            <img src="{{asset('/images/slide1.jpg')}}" alt="" class="d-block w-100">
                        </div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>PENGIRIMAN LAUT TERPERCAYA</h3>
                            <p>SDS Logistics bergerak di bidang Jasa Pengiriman Laut dengan pengalaman lebih dari 50 tahun dan didukung tim yang kompeten.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-img">
                            <img src="{{asset('/images/slide2.jpg')}}" alt="" class="d-block w-100">
                        </div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>GUDANG SUNTER</h3>
                            <p>Dengan luas 1500 m2 dan lokasi yang dekat dengan pelabuhan, SDS Logistics dapat menjamin ketepatan waktu pengiriman barang.</p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <div class="carousel-img">
                            <img src="{{asset('/images/slide3.jpg')}}" alt="" class="d-block w-100">
                        </div>
                        <div class="carousel-caption d-none d-md-block">
                            <h3>GUDANG MARUNDA</h3>
                            <p>Dengan luas lebih dari 1 hektar, kami dapat menampung order pengiriman barang dalam kapasitas besar.</p>
                        </div>
                    </div>
                    <a href="#homeCarousel" class="carousel-control-prev" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a href="#homeCarousel" class="carousel-control-next" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="row tarif-row">
        <div class="col tarif-col">
            <div class="container cek-tarif">
                <div class="row">
                    <div class="col">
                        <h3>Cek Tarif Pengiriman</h3>
                        <hr>
                    </div>
                </div>
                <div class="form-row">
                    <div class="col">
                        <form action="" class="form-inline">
                            <input type="number" name="kuantitas" id="kuantitas" class="form-control mb-2 mr-sm-2" data-toggle="tooltip" data-placement="bottom" title="Kuantitas" placeholder="Kuantitas">
                            <select name="satuan" id="satuan" class="form-control mb-2 mr-sm-2" data-toggle="tooltip" data-placement="bottom" title="Satuan">
                                <option value="m3">M<sup>3</sup></option>
                                <option value="kg" disabled>Kg</option>
                            </select>
                            <select name="kotaTujuan" id="kotaTujuan" class="form-control mb-2 mr-sm-2" data-toggle="tooltip" data-placement="bottom" title="Kota Tujuan">
                                <option value="makassar">Makassar</option>
                                <option value="manado">Manado</option>
                                <option value="banjarmasin">Banjarmasin</option>
                                <option value="samarinda">Samarinda</option>
                                <option value="sorong">Sorong</option>
                                <option value="biak">Biak</option>
                                <option value="jayapura">Jayapura</option>
                                <option value="merauke">Merauke</option>
                                <option value="timika">Timika</option>
                                <option value="manokwari">Manokwari</option>
                                <option value="palu">Palu</option>
                                <option value="balikpapan">Balikpapan</option>
                            </select>
                            <div class="mb-2 mr-sm-2" id="biaya"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-body">
                        <h1><i class="fa fa-users card-icon"></i></h1>
                        <h5 class="card-title">TIM YANG BERPENGALAMAN</h5>
                        <p class="card-text">Keberhasilan pengiriman barang didukung oleh koordinasi tim yang baik, solid dan berpengalaman.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-body">
                        <h1><i class="fa fa-handshake-o card-icon"></i></h1>
                        <h5 class="card-title">CUSTOMER SERVICE 24/7</h5>
                        <p class="card-text">Kami selalu siap & sigap menangani setiap permasalahan pengiriman barang anda setiap saat.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card text-center">
                    <div class="card-body">
                        <h1><i class="fa fa-tag card-icon"></i></h1>
                        <h5 class="card-title">HARGA TERBAIK & KOMPETITIF</h5>
                        <p class="card-text">Setiap barang yang akan dikirim akan diukur dan dihitung secara real & transparan.</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <h1 class="ourpartner">PARTNER KAMI</h1>
                <hr class="bluesds">
                <div class="row">
                    <div class="col"><img src="{{asset('/images/partner1.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/partner2.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/partner3.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/partner4.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/partner5.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/partner6.png')}}" alt="" class="w-100"></div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <h1 class="ourclient">KLIEN KAMI</h1>
                <hr class="bluesds">
                <div class="row">
                    <div class="col"><img src="{{asset('/images/klien1.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/klien2.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/klien3.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/klien4.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/klien5.png')}}" alt="" class="w-100"></div>
                    <div class="col"><img src="{{asset('/images/klien6.png')}}" alt="" class="w-100"></div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col">
                <h1 class="video-title">VIDEO</h1>
                <hr class="bluesds">
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <video width="360" height="360" controls>
                            <source src="{{asset('/video/home-video.mp4')}}" type="video/mp4">
                        </video>
                    </div>
                    <div class="col-md-4"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- Success modal -->
    @if(isset($success))
    <div class="modal" tabindex="-1" role="dialog" aria-hidden="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Order berhasil</h5>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-content">
                    <h4>{{$success}}</h4>
                </div>
                <div class="modal-footer"><button class="btn btn-primary" data-dismiss="modal">Oke</button></div>
            </div>
        </div>
    </div>
    @endif

    <!-- Order modal -->
    <div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{route('sendmail-order')}}" method="post">
            @csrf
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Order pengiriman</h5>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group"><input type="text" name="namalengkap" class="form-control" placeholder="Nama lengkap"></div>
                    <div class="form-group"><input type="tel" name="telepon" class="form-control" placeholder="No. Telepon"></div>
                    <div class="form-group"><input type="tel" name="hpwa" class="form-control" placeholder="No. HP / Whatsapp"></div>
                    <div class="form-group"><input type="text" name="email" class="form-control" placeholder="E-Mail"></div>
                    <div class="form-group"><input type="text" name="jenisbarang" class="form-control" placeholder="Jenis Barang"></div>
                    <div class="form-group">Dimensi barang (m<sup>3</sup> ) : <input type="number" name="dimensi" class="form-control"></div>
                    <h5>Alamat Tujuan</h5>
                    <hr>
                    <div class="form-group"><textarea name="alamattujuan" cols="32" rows="5" class="form-control"></textarea></div>
                    <div class="form-group form-row"><div class="col"><input type="text" name="kecamatantujuan" class="form-control" placeholder="Kecamatan"></div><div class="col"><input type="number" name="kodepostujuan" class="form-control" placeholder="Kode Pos"></div></div>
                    <h5>Alamat Pickup</h5>
                    <hr>
                    <div class="form-group"><textarea name="alamatpickup" cols="32" rows="5" class="form-control"></textarea></div>
                    <div class="form-group form-row"><div class="col"><input type="text" name="kecamatanpickup" class="form-control" placeholder="Kecamatan"></div><div class="col"><input type="number" name="kodepospickup" class="form-control" placeholder="Kode Pos"></div></div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-danger" data-dismiss="modal">Cancel <span class="fa fa-times"></span></button>
                    <button type="submit" class="btn btn-success">Pesan <span class="fa fa-shopping-cart"></span></button>
                </div>
            </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
        
        $("#kuantitas,#satuan,#kotaTujuan").change(function(){
            kuantitas = $("#kuantitas").val();
            satuan = $("#satuan").val();
            kotaTujuan = $("#kotaTujuan").val();
            switch(kotaTujuan){
                case "makassar" : hargaKg = 650; hargam3 = 500000; break;
                case "manado" : hargaKg = 715; hargam3 = 560000; break;
                case "banjarmasin" : hargaKg = 595; hargam3 = 455000; break;
                case "samarinda" : hargaKg = 675; hargam3 = 525000; break;
                case "sorong" : hargaKg = 0; hargam3 = 1100000; break;
                case "biak" : hargaKg = 0; hargam3 = 1500000; break;
                case "jayapura" : hargaKg = 0; hargam3 = 1200000; break;
                case "merauke" : hargaKg = 0; hargam3 = 1500000; break;
                case "timika" : hargaKg = 0; hargam3 = 1500000; break;
                case "manokwari" : hargaKg = 0; hargam3 = 1500000; break;
                case "palu" : hargaKg = 0; hargam3 = 750000; break;
                case "balikpapan" : hargaKg = 0; hargam3 = 700000; break;
            }
            if(satuan == 'kg' && kuantitas < 1000){
                alert("Kuantitas minimal 1.000 Kg");
            }else{
                if(satuan == 'm3'){
                        harga = new Number(kuantitas*hargam3).toLocaleString('id-ID');
                        $('input[name=dimensi]').val(kuantitas);
                        harga = 'Rp ' + harga + ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-light" data-toggle="modal" data-target="#modalOrder">Order sekarang</button>';
                }else if(satuan == 'kg'){
                    if(hargam3 == 0){
                        harga = 'Tidak tersedia'
                    }else{
                        harga = new Number(kuantitas*hargaKg).toLocaleString('id-ID');
                        harga = 'Rp ' + harga + ' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-light" data-toggle="modal" data-target="#modalOrder">Order sekarang</button>';
                    }
                }

                $("#biaya").html( harga ).show();
            }
        });
    });
</script>
@endsection