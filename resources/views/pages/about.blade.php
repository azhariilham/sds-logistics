@extends('main')

@section('styles')
    
@endsection

@section('content')
<div class="row">
    <div class="col">
        <img src="{{asset('images/aboutimg.jpg')}}" alt="Tentang Kami" class="d-block w-100">
        <div class="container">
            <div class="mb-5"></div>
            <h1>Tentang Kami</h1>
            <p>SDS Logistics adalah perusahaan yang bergerak di bidang logistik khususnya pengiriman laut, yang memiliki pengalaman kurang lebih selama 50 tahun. Didukung oleh tin yang berpengalaman dan berdedikasi tinggi, kami menjamin setiap pengiriman sampai tujuan tanpa kendala, demi menjaga kredibilitas dan integritas.
            </p>
            <p>Dengan dukungan Infrastruktur 2 gudang penyimpanan barang yang terdiri dari :</p>
            <ul>
                <li>Sunter – Jakarta Utara dengan luas lebih dari 1500m2</li>
                <li>Marunda – Jakarta utara dengan luas lebih dari 1 hektar</li>
            </ul>
            <p>Serta armada trailer sebanyak 50 unit.</p>
            <p>Kami menyediakan layanan LCL (Less Container Load) dan FCL (Full Container Load) dengan harga yang sangat kompetitif.</p>
            <video width="360" height="360" controls>
                <source src="{{asset('/video/about-video.mp4')}}" type="video/mp4">
            </video>
        </div>
    </div>
</div>
    
@endsection