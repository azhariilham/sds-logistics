@extends('main')

@section('content')
    <div class="container" style="min-height: 65vh;">
        <br><br><br><br>
        <form action="#" class="form-inline form-track-order"><div class="input-group"><input type="text" name="searchOrder" id="searchOrder" class="form-control" style="min-width: 65vw;" placeholder="Masukan kode order (Cth: SDS-0000000)"><div class="input-group-append"><button type="submit" class="btn btn-outline-primary"><i class="fa fa-search"></i> Track order</button></div></div></form>
        <br><br>
        <table class="table table-striped">
            <thead><tr><th>Waktu</th><th>Status</th></tr></thead>
            <tbody class="tracking-table"></tbody>
        </table>
    </div>
@endsection

@section('scripts')
<script>
    $(function(){
        @if(isset($order))
        $.ajax({
            url: "{{route('trackingdata')}}",
            method: "POST",
            data: {
                _token: '{{csrf_token()}}',
                order: '{{$order}}'
            },
            success: function(data){
                $('.tracking-table').empty();
                $.each(data, function(i,item){
                    $('.tracking-table').append('<tr><td>'+data[i].created_at+'</td><td>'+data[i].status+'</td></tr>');
                });
            }
        });
        @endif
        $('.form-track-order').submit(function(e){
            e.preventDefault();
            $.ajax({
                url: "{{route('trackingdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    order: $('#searchOrder').val()
                },
                success: function(data){
                    $('.tracking-table').empty();
                    $.each(data, function(i,item){
                        $('.tracking-table').append('<tr><td>'+data[i].created_at+'</td><td>'+data[i].status+'</td></tr>');
                    });
                }
            });
        });
    });
</script>    
@endsection