@extends('main')

@section('styles')
    
@endsection

@section('content')

<div class="container">
    <div class="mb-5"></div>
    <h1>Tarif</h1>
    <div class="mb-5"></div>
    <h3>Untuk  M<sup>3</sup></h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">KOTA TUJUAN</th>
                <th scope="col">HARGA</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>MAKASSAR</td>
                <td>Rp 500.000</td>
            </tr>
            <tr>
                <td>MANADO</td>
                <td>Rp 560.000</td>
            </tr>
            <tr>
                <td>BANJARMASIN</td>
                <td>Rp 455.000</td>
            </tr>
            <tr>
                <td>BALIKPAPAN</td>
                <td>Rp 700.000</td>
            </tr>
            <tr>
                <td>SAMARINDA</td>
                <td>Rp 525.000</td>
            </tr>
            <tr>
                <td>PALU - SULAWESI</td>
                <td>Rp 750.000</td>
            </tr>
            <tr>
                <td>SORONG - PAPUA</td>
                <td>Rp 1.100.000</td>
            </tr>
            <tr>
                <td>BIAK - PAPUA</td>
                <td>Rp 1.500.000</td>
            </tr>
            <tr>
                <td>JAYAPURA - PAPUA</td>
                <td>Rp 1.200.000</td>
            </tr>
            <tr>
                <td>MERAUKE - PAPUA</td>
                <td>Rp 1.500.000</td>
            </tr>
            <tr>
                <td>TIMIKA - PAPUA</td>
                <td>Rp 1.500.000</td>
            </tr>
            <tr>
                <td>MANOKWARI - PAPUA</td>
                <td>Rp 1.500.000</td>
            </tr>
        </tbody>
    </table>
    <div class="mb-5"></div>

    <!--<h3>Untuk Kg</h3>
    <table class="table">
        <thead>
            <tr>
                <th scope="col">KOTA TUJUAN</th>
                <th scope="col">HARGA</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>MAKASSAR</td>
                <td>Rp 650.000</td>
            </tr>
            <tr>
                <td>MANADO</td>
                <td>Rp 715.000</td>
            </tr>
            <tr>
                <td>BANJARMASIN</td>
                <td>Rp 595.000</td>
            </tr>
            <tr>
                <td>SAMARINDA</td>
                <td>Rp 675.000</td>
            </tr>
        </tbody>
    </table>-->
</div>
    
@endsection