@extends('admin.layout.main')

@section('title', 'Tracking')

@section('content')
<h1>Tracking</h1>

<form action="{{route('admin.trackingdata')}}" method="post" class="form-inline form_trackorder">
    <div class="input-group mb-6">
        <input type="text" name="searchOrder" id="searchOrder" class="form-control" placeholder="Enter order code (Ex: SDS-XXX)" style="min-width: 360px;">
        <div class="input-group-append"><button type="submit" class="btn btn-outline-secondary"><i class="fa fa-search"></i> Find order</button></div>
    </div>
</form>
<br><br>
<h4>Order code : <span class="order_code">@if(isset($order)) {{$order}} @endif</span></h4>
<table class="table table-striped">
    <thead><tr><th>Time</th><th>Status</th></tr></thead>
    <tbody class="tracking-table"></tbody>
</table>
@endsection

@section('scripts')
<script>
    function addTracking(order){
            status = $('#addStatus').val();
            $.ajax({
                url: "{{route('admin.trackingdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'add',
                    order: order,
                    status: status
                },
                success: function(data){
                    window.location = "{{route('admin.tracking')}}/"+order;
                }
            });
        }

        function removeTracking(order){
            $.ajax({
                url: "{{route('admin.trackingdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'remove',
                    order: order
                },
                success: function(data){
                    window.location = "{{route('admin.tracking')}}/"+order;
                }
            });
        }

    $(function(){
        @if(isset($order))
        $.ajax({
            url: "{{route('admin.trackingdata')}}",
            method: "POST",
            data: {
                _token: '{{csrf_token()}}',
                op: 'view',
                order: '{{$order}}'
            },
            success: function(data){
                $('.tracking-table').empty();
                $.each(data, function(i,item){
                    $('.tracking-table').append('<tr><td>'+data[i].date+'</td><td>'+data[i].status+'</td></tr>');
                });
                $('.tracking-table').append('<tr><td></td><td><div class="input-group mb-6"><input type="text" name="addStatus" id="addStatus" class="form-control"><div class="input-group-append"><button type="button" class="btn btn-outline-secondary" onclick="addTracking(\'{{$order}}\');"><i class="fa fa-plus"></i> Add Status</button></div></div></td></tr>');
            }
        });
        @endif
        $('.form_trackorder').submit(function(e){
            e.preventDefault();
            $('.order_code').html($('#searchOrder').val());
            $.ajax({
                url: "{{route('admin.trackingdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'view',
                    order: $('#searchOrder').val()
                },
                success: function(data){
                    $('.tracking-table').empty();
                    $.each(data, function(i,item){
                        $('.tracking-table').append('<tr><td>'+data[i].date+'</td><td>'+data[i].status+'</td></tr>');
                    });
                    $('.tracking-table').append('<tr><td></td><td><div class="input-group mb-6"><input type="text" name="addStatus" id="addStatus" class="form-control"><div class="input-group-append"><button type="button" class="btn btn-outline-secondary" onclick="addTracking(\''+$('#searchOrder').val()+'\');"><i class="fa fa-plus"></i> Add Status</button></div></div></td></tr>');
                }
            });
        });

    });
</script>    
@endsection