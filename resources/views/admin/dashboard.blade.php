@extends('admin.layout.main')

@section('title', 'Dashboard')

@section('content')

<h1>Dashboard</h1>

<table class="table table-bordered">
    <tr>
        <td><i class="fa fa-file"></i> New Order : {{$newOrder}}</td>
        <td><i class="fa fa-refresh"></i> Processed Order : {{$processedOrder}}</td>
    </tr>
    <tr>
        <td><i class="fa fa-truck"></i> On Delivery Order : {{$onDeliveryOrder}}</td>
        <td><i class="fa fa-check"></i> Delivered Order : {{$deliveredOrder}}</td>
    </tr>
</table>
    
@endsection