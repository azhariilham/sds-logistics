@extends('admin.layout.main')

@section('title', $title)

@section('style')
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css">
@endsection

@section('content')
<h1>{{$title}}</h1>

<table class="table table-striped" id="orderTable">
    <thead class="head-dark">
        <th>No.</th>
        <th>Client Name</th>
        <th>Jenis Barang</th>
        <th>Berat</th>
        <th>Ukuran</th>
        <th>Tanggal Penerimaan</th>
        <th>Penerima</th>
        <th>Status Penerimaan</th>
        <th>Dicek Oleh</th>
        <th>Tujuan</th>
        <th>Update Barang</th>
        <th>No. Resi</th>
        <th>Status Pembayaran</th>
        <th>Action</th>
    </thead>
    <tbody>
        @php
            $row = 0;
        @endphp
        @foreach ($order as $ord)
        @php
            $detail = json_decode($ord->order_details);
            $contact = json_decode($ord->contact_person);
            $pickup = json_decode($ord->pickup);
            $destination = json_decode($ord->destination);
        @endphp
            <tr>
                <td>{{$row+=1}}</td>
                <td>{{$contact->namalengkap}}</td>
                <td>{{$detail->jenisbarang}}</td>
                <td></td>
                <td>{{$detail->dimensi}} m<sup>3</sup></td>
                <td>{{$ord->receipt_date}}</td>
                <td>{{$ord->accepted_by}}</td>
                <td>{{$ord->acceptance_status}}</td>
                <td>{{$ord->checked_by}}</td>
                <td>{{$destination->kecamatan}}</td>
                <td>{{$ord->status()}}</td>
                <td>{{$ord->order_code}}</td>
                <td>{{$ord->payment_status}}</td>
                <td>
                    <span data-toggle="tooltip" data-placement="top" title="Order details"><button type="button" class="btn btn-primary btn-order-details" data-order="{{$ord->order_code}}" data-toggle="modal" data-target="#modalDetails" data-order="{{$ord->id}}"><i class="fa fa-file-text"></i></button></span>
                    <button type="button" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Remove order" onclick="removeOrder('{{$ord->order_code}}')"><i class="fa fa-times"></i></button>
                </td>
            </tr>
            
        @endforeach
    </tbody>
</table>

<div class="modal fade" id="modalDetails" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Order Details</h5>
                <button class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body" id="modalDetailsBody">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-set-tracking"></button> <button type="button" class="btn btn-success btn-change-status"></button> <button type="button" class="btn btn-info btn-set-payment"></button> <button class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
            </div>
        </div>
    </div>
</div>

<br>
<span data-toggle="tooltip" data-placement="top" title="Order details"><button class="btn btn-success" data-toggle="modal" data-target="#modalOrder"><i class="fa fa-plus"></i></button></span>
<div class="modal fade" id="modalOrder" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <form action="{{route('sendmail-order')}}" method="post">
        @csrf
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Order pengiriman</h5>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="form-group"><input type="text" name="namalengkap" class="form-control" placeholder="Nama lengkap"></div>
                <div class="form-group"><input type="tel" name="telepon" class="form-control" placeholder="No. Telepon"></div>
                <div class="form-group"><input type="tel" name="hpwa" class="form-control" placeholder="No. HP / Whatsapp"></div>
                <div class="form-group"><input type="text" name="email" class="form-control" placeholder="E-Mail"></div>
                <div class="form-group"><input type="text" name="jenisbarang" class="form-control" placeholder="Jenis Barang"></div>
                <div class="form-group">Dimensi barang (m<sup>3</sup> ) : <input type="number" name="dimensi" class="form-control"></div>
                <h5>Alamat Tujuan</h5>
                <hr>
                <div class="form-group"><textarea name="alamattujuan" cols="32" rows="5" class="form-control"></textarea></div>
                <div class="form-group form-row"><div class="col"><input type="text" name="kecamatantujuan" class="form-control" placeholder="Kecamatan"></div><div class="col"><input type="number" name="kodepostujuan" class="form-control" placeholder="Kode Pos"></div></div>
                <h5>Alamat Pickup</h5>
                <hr>
                <div class="form-group"><textarea name="alamatpickup" cols="32" rows="5" class="form-control"></textarea></div>
                <div class="form-group form-row"><div class="col"><input type="text" name="kecamatanpickup" class="form-control" placeholder="Kecamatan"></div><div class="col"><input type="number" name="kodepospickup" class="form-control" placeholder="Kode Pos"></div></div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" data-dismiss="modal">Cancel <span class="fa fa-times"></span></button>
                <button type="submit" class="btn btn-success">Pesan <span class="fa fa-shopping-cart"></span></button>
            </div>
        </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
<script>
    function removeOrder(order){
            $.ajax({
                url: "{{route('admin.orderdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'remove',
                    order: order
                },
                success: function(){
                    location.reload();
                }
            });
        }
        
    $(function () {

    $('#orderTable').DataTable({
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        columnDefs: [
            { width: "40px", targets: 0 },
            { width: "180px", targets: 1 },
            { width: "180px", targets: 2 },
            { width: "80px", targets: 3 },
            { width: "80px", targets: 4 },
            { width: "180px", targets: 5 },
            { width: "180px", targets: 6 },
            { width: "180px", targets: 7 },
            { width: "180px", targets: 8 },
            { width: "180px", targets: 9 },
            { width: "180px", targets: 10 },
            { width: "180px", targets: 11 },
            { width: "180px", targets: 12 },
            { width: "180px", targets: 13 },
        ]
    });

        $('.btn-set-tracking').click(function(){
            window.location = "{{route('admin.tracking')}}/"+$(this).data('order');
        });

        $('.btn-change-status').click(function(){
            $.ajax({
                url: "{{route('admin.orderdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'set-status',
                    order: $(this).data('order'),
                    status: $(this).data('status')
                },
                success: function(){ location.reload(); }
            });
        });

        $('.btn-set-payment').click(function(){
            $.ajax({
                url: "{{route('admin.orderdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'set-payment',
                    order: $(this).data('order')
                }
                //success: function(){ location.reload(); }
            })
        })

        $('.btn-order-details').click(function(){
            order = $(this).data('order');
            $.ajax({
                url: "{{route('admin.orderdata')}}",
                method: "POST",
                data: {
                    _token: '{{csrf_token()}}',
                    op: 'details',
                    order: order
                },
                success: function(data){
                    details = JSON.parse(data.order_details);
                    pickup = JSON.parse(data.pickup);
                    destination = JSON.parse(data.destination);
                    cp = JSON.parse(data.contact_person);
                    $('#modalDetailsBody').html('<table class="table">'
                        +'<tr><th>Kode Order</th><td>'+data.order_code+'</td><tr>'
                        +'<tr><th>Order Date</th><td>'+data.created_at+'</td><tr>'
                        +'<tr><th>Nama lengkap</th><td>'+cp.namalengkap+'</td><tr>'
                        +'<tr><th>E-Mail</th><td><a href="mailto:'+cp.email+'" target="_blank">'+cp.email+'</a></td><tr>'
                        +'<tr><th>Telepon</th><td><a href="tel:'+cp.telepon+'" target="_blank">'+cp.telepon+'</a></td><tr>'
                        +'<tr><th>HP / WA</th><td><a href="https://api.whatsapp.com/send?phone='+cp.hpwa+'" target="_blank">'+cp.hpwa+'</a></td><tr>'
                        +'<tr><th>Jenis Barang</th><td>'+details.jenisbarang+'</td><tr>'
                        +'<tr><th>Dimensi</th><td>'+details.dimensi+' m<sup>3</sup></td><tr>'
                        +'<tr><th>Pickup</th><td>'+pickup.alamat+' Kec. '+pickup.kecamatan+' Kode pos '+pickup.kodepos+'</td><tr>'
                        +'<tr><th>Destination</th><td>'+destination.alamat+' Kec. '+destination.kecamatan+' Kode pos '+destination.kodepos+'</td><tr>'
                        +'</table>');
                        switch(data.order_status){
                            case 1: status = '<i class="fa fa-truck"></i> Wh/Port'; break;
                            case 2: status = '<i class="fa fa-ship"></i> On delivery'; break;
                            case 3: status = '<i class="fa fa-industry"></i> Destination port'; break;
                            case 4: status = '<i class="fa fa-check"></i> Delivered'; break;
                            case 5: status = ''; break;
                        }
                        if(data.order_status == 0){
                            $('.btn-change-status').html('<i class="fa fa-refresh"></i> Set on process');
                            $('.btn-set-tracking').hide();
                        }else if(data.order_status > 0 && data.order_status <= 4){
                            $('.btn-set-tracking').show();
                            $('.btn-change-status').show();
                            $('.btn-set-tracking').attr('data-order', data.order_code);
                            $('.btn-set-tracking').html('<i class="fa fa-map-marker"></i> Set tracking');
                            $('.btn-change-status').html(status);
                        }else{
                            $('.btn-change-status').hide(); $('.btn-set-tracking').hide();
                        }

                        if(data.payment_status == null){
                            $('.btn-set-payment').show();
                            $('.btn-set-payment').attr('data-order', data.order_code);
                            $('.btn-set-payment').html('<i class="fa fa-money"></i> Pay');
                        }else{
                            $('.btn-set-payment').hide();
                        }
                        $('.btn-set-tracking').attr('data-order', data.order_code);
                        $('.btn-change-status').attr('data-order', data.order_code);
                        $('.btn-change-status').attr('data-status', data.order_status+1);
                        
                }
            });
        });

    });
</script>
@endsection