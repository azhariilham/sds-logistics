<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
		<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>SDS Logistics - @yield('title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

		<!-- Styles -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<style type="text/css">@import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700;900&display=swap'); body{font-family: 'Roboto', serif !important; min-height: 100%;} .container-fluid, .admin-layout, .sidebar-sticky{min-height: 100vh;} .nav-link{color: #555; transition: .5s; } .nav-link:hover{ color: #999; } .sidebar-sticky{border-right: 1px solid #999;}
        </style>
        @yield('style')
  </head>
  <body>
    
    <div class="container-fluid fill">
        <div class="row admin-layout">
            <div class="col-md-2">
                <div class="sidebar-sticky">
                    <a href="#" class="navbar-brand"><img src="{{ url('/images/sds-logo.jpg') }}" width="154" height="41" alt="" srcset=""></a>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a href="{{route('admin.dashboard')}}" class="nav-link"><i class="fa fa-home"></i> Dashboard</a></li>
                    </ul>
                    <hr>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a href="{{route('admin.order')}}" class="nav-link"><i class="fa fa-file"></i> Order</a></li>
                    </ul>
                    <hr>
                    <ul class="nav flex-column">
                      <li class="nav-item"><a href="{{route('admin.tracking')}}" class="nav-link"><i class="fa fa-map-marker"></i> Tracking</a></li>
                    </ul>
                    <hr>
                    <ul class="nav flex-column">
                        <li class="nav-item"><a href="{{route('logout')}}" class="nav-link"><i class="fa fa-sign-out"></i> Log out</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-10" id="app">
                @yield('content')
            </div>
        </div>
    </div>

  <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
  <script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip();
      $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
  </script>
  @yield('scripts')
  </body>
</html>
