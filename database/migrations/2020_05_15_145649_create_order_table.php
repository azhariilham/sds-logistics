<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            $table->id();
            $table->string('order_code'); // No. Resi
            $table->json('order_details');
            $table->date('receipt_date');
            $table->string('accepted_by')->nullable();
            $table->string('acceptance_status')->nullable();
            $table->string('checked_by')->nullable();
            $table->json('pickup');
            $table->json('destination');
            $table->json('contact_person');
            $table->integer('order_status')->comment('0: Order baru; 1: Order diproses; 2: Gudang / Pelabuhan Jakarta; 3: Berlayar; 4: Pelabuhan Tujuan; 5: Sampai Tujuan;');
            $table->string('payment_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order');
    }
}
