<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /*
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'adminsunter',
            'credentials' => 'Administrator',
            'password' => Hash::make('sDs041160#!'),
        ]);
    }
}
