<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    const STATUS_NEW = 0;
    const STATUS_PROCESSED = 1;
    const STATUS_WAREHOUSE = 2;
    const STATUS_ON_DELIVERY = 3;
    const STATUS_DESTINATION_PORT = 4;
    const STATUS_DELIVERED = 5;

    protected $table = 'order';
    protected $fillable = ['order_code', 'order_details', 'pickup', 'destination', 'contact_person', 'order_status'];

    public function Tracking()
    {
        $this->hasMany('App\Tracking');
    }

    public function status()
    {
        switch($this->order_status){
            case self::STATUS_NEW: $status = 'Order baru'; break;
            case self::STATUS_PROCESSED: $status = 'Order diproses'; break;
            case self::STATUS_WAREHOUSE: $status = 'Gudang / pelabuhan Jakarta'; break;
            case self::STATUS_ON_DELIVERY: $status = 'Dalam pengiriman'; break;
            case self::STATUS_DESTINATION_PORT: $status = 'Pelabuhan tujuan'; break;
            case self::STATUS_DELIVERED: $status = 'Terkirim'; break;
        }
        return $status;
    }
}
