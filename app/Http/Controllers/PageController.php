<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OrderMail;
use Livewire\Component;
use Illuminate\Support\Facades\Mail;
use App\Order;
use App\Tracking;

class PageController extends Controller
{
    public function sendOrder(Request $request)
    {
        $orderObj = new \stdClass();
        $orderObj->namalengkap = $request->namalengkap;
        $orderObj->telepon = $request->telepon;
        $orderObj->hpwa = $request->hpwa;
        $orderObj->email = $request->email;
        $orderObj->jenisbarang = $request->jenisbarang;
        $orderObj->dimensi = $request->dimensi;
        $orderObj->alamattujuan = $request->alamattujuan;
        $orderObj->kecamatantujuan = $request->kecamatantujuan;
        $orderObj->kodepostujuan = $request->kodepostujuan;
        $orderObj->alamatpickup = $request->alamatpickup;
        $orderObj->kecamatanpickup = $request->kecamatanpickup;
        $orderObj->kodepospickup = $request->kodepospickup;

        $order_num = Order::count()+1;
        $order_code = 'SDS-'.str_pad($order_num, 7, "0", STR_PAD_LEFT);

        $order = new Order;
        $order->order_code = $order_code;
        $order->receipt_date = date('Y-m-d');
        $order->order_details = '{ "jenisbarang": "'.$request->jenisbarang.'", "dimensi": "'.$request->dimensi.'" }';
        $order->pickup = '{ "alamat": "'.$request->alamatpickup.'", "kecamatan": "'.$request->kecamatanpickup.'", "kodepos": "'.$request->kodepospickup.'"}';
        $order->destination = '{"alamat": "'.$request->alamattujuan.'", "kecamatan": "'.$request->kecamatantujuan.'", "kodepos": "'.$request->kodepostujuan.'"}';
        $order->contact_person = '{"namalengkap": "'.$request->namalengkap.'", "email": "'.$request->email.'", "telepon": "'.$request->telepon.'", "hpwa": "'.$request->hpwa.'"}';
        $order->order_status = 0;
        $order->save();

        Mail::to("sds.logisticsindo@gmail.com")->send(new OrderMail($orderObj));

        return redirect()->back()->with('success', 'Order anda berhasil, dan akan segera kami proses');
    }

    public function trackOrder(Request $request)
    {
        $order = Order::where('order_code', $request->order)->first();
        $tracking = Tracking::where('order_id', $order['id'])->get();
        return response()->json($tracking);
    }
}
