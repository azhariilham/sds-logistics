<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use App\Tracking;

class AdminController extends Controller
{
    public function dashboard()
    {
        $newOrder = Order::where('order_status',0)->count();
        $processedOrder = Order::where('order_status',1)->count();
        $onDeliveryOrder = Order::where('order_status',2)->count();
        $deliveredOrder = Order::where('order_status',3)->count();

        return view('admin.dashboard', ['newOrder' => $newOrder, 'processedOrder' => $processedOrder, 'onDeliveryOrder' => $onDeliveryOrder, 'deliveredOrder' => $deliveredOrder]);
    }

    public function order()
    {
        return view('admin.order', ['order' => Order::get(), 'title' => 'Order']);
    }

    public function orderData(Request $request)
    {
        $order = Order::where('order_code', $request->order);
        switch($request->op){
            case 'details': return response()->json($order->first()); break;
            case 'set-status': $order->update(['order_status' => $request->status]); $tracking = new Tracking; $orderFirst = $order->first(); $tracking->order_id = $orderFirst['id']; $tracking->status = $orderFirst->status(); $tracking->save(); return response()->json(['resp' => 'ok']); break;
            case 'set-payment': $order->update(['payment_status' => 'Paid']); return response()-> json(['resp' => 'ok']); break;
            case 'remove': Order::where('order_code', $request->order)->delete(); return response()->json(['resp' => 'ok']); break;
        }
    }

    public function tracking(Request $request)
    {
        $order = Order::where('order_code', $request->order)->first();
        switch($request->op){
            case 'view': return response()->json(Tracking::selectRaw("order_id, status, DATE_FORMAT(created_at, '%e %M %Y %H:%i') as date")->where('order_id', $order['id'])->get()); break;
            case 'add': $tracking = new Tracking; $tracking->order_id = $order['id']; $tracking->status = $request->status; $tracking->save(); return response()->json(['resp' => 'ok']); break;
            case 'remove': $tracking = Tracking::where('id', $request->trackingId)->delete(); return response()->json(['resp' => 'ok']); break;
        }
    }
}
