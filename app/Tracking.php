<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    protected $table = 'tracking';
    protected $fillable = ['order_id', 'status'];

    public function Order()
    {
        $this->belongsTo('App\Order');
    }
}
