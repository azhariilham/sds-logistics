<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () { return view('pages.home'); })->name('home');
Route::get('/about', function () { return view('pages.about'); })->name('about');
Route::get('/tarif', function () { return view('pages.tarif'); })->name('tarif');
Route::get('/tracking', function (){ return view('pages.tracking'); })->name('tracking');
Route::get('/tracking/{order}', function ($order){ return view('pages.tracking', ['order' => $order]); })->name('ordertracking');
Route::post('/tracking', 'PageController@trackOrder')->name('trackingdata');
Route::post('/sendmail-order', 'PageController@sendOrder')->name('sendmail-order');

// Login user
Route::get('/login', function () { return view('auth.login'); })->name('login');
Route::get('/logout', function(){ Auth::logout(); return redirect('/'); })->name('logout');

Auth::routes();

Route::group(['prefix' => 'administration', 'middleware' => 'auth', 'layout' => 'admin.layout.main'],function(){
    Route::get('/', 'AdminController@dashboard')->name('admin.dashboard');
    Route::get('/order', 'AdminController@order')->name('admin.order');
    Route::post('/order-data', 'AdminController@orderData')->name('admin.orderdata');
    Route::get('/tracking', function(){ return view('admin.tracking'); })->name('admin.tracking');
    Route::get('/tracking/{order}', function($order){ return view('admin.tracking', ['order' => $order]); })->name('admin.tracking_find');
    Route::post('/tracking-data', 'AdminController@tracking')->name('admin.trackingdata');
});

